import React, { createContext, useState, useEffect } from 'react';
import Axios from 'axios';

export const RecetasContext = createContext();

const RecetasProvider = (props) => {
  const [recetas, setRecetas] = useState([]);
  const [busquedaReceta, setBusquedaReceta] = useState({
    ingrediente: '',
    categoria: '',
  });
  useEffect(() => {
    const obtenerRecetas = async () => {
      if (busquedaReceta.ingrediente === '') {
        return;
      }
      const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${busquedaReceta.ingrediente}&c=${busquedaReceta.categoria}`;
      const recetas = await Axios(url);
      setRecetas(recetas.data.drinks);
    };
    obtenerRecetas();
  }, [busquedaReceta]);
  return (
    <RecetasContext.Provider value={{ recetas, setBusquedaReceta }}>
      {props.children}
    </RecetasContext.Provider>
  );
};

export default RecetasProvider;
