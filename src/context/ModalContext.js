import React, { useEffect, useState, createContext } from 'react';
import Axios from 'axios';

//crear context
export const ModalContext = createContext();

const ModalProvider = (props) => {
  //state del provider
  const [idreceta, setIdreceta] = useState(null);
  const [unareceta, setUnareceta] = useState({});

  //llamar api con id
  useEffect(() => {
    const obtenerReceta = async () => {
      if (idreceta === null) {
        return;
      }
      const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idreceta}`;
      const resultado = await Axios(url);
      setUnareceta(resultado.data.drinks[0]);
    };
    obtenerReceta();
  }, [idreceta]);

  return (
    <ModalContext.Provider value={{ unareceta, setIdreceta, setUnareceta }}>
      {props.children}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
