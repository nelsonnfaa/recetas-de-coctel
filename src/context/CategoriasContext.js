import React, { createContext, useState, useEffect } from 'react';
import Axios from 'axios';

//crear context
export const CategoriasContext = createContext();

//provider es donde se encuantran las funciones y state
const CategoriasProvider = (props) => {
  const [categorias, setCategorias] = useState([]);
  useEffect(() => {
    const obtenerCategorias = async () => {
      const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';
      const categorias = await Axios(url);
      setCategorias(categorias.data.drinks);
    };
    obtenerCategorias();
  }, []);
  return (
    <CategoriasContext.Provider
      value={{
        categorias,
      }}
    >
      {props.children}
    </CategoriasContext.Provider>
  );
};

export default CategoriasProvider;
