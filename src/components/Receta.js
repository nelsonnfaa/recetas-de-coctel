import React, { useContext, useState } from 'react';
import { ModalContext } from '../context/ModalContext';

import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Receta = ({ receta }) => {
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const { unareceta, setIdreceta, setUnareceta } = useContext(ModalContext);

  const { strDrink, strDrinkThumb, idDrink } = receta;

  const mostrarIngredientes = (unareceta) => {
    let ingredientes = [];
    for (let i = 1; i < 16; i++) {
      if (unareceta[`strIngredient${i}`]) {
        ingredientes.push(
          <li>
            {unareceta[`strIngredient${i}`]} = 
            {unareceta[`strMeasure${i}`]}
          </li>
        );
      }
    }
    return ingredientes;
  };

  return (
    <div className="col-md-4 mb-3">
      <div className="card">
        <h2 className="card-header">{strDrink}</h2>
        <img src={strDrinkThumb} alt={strDrinkThumb} className="card-img-top" />
        <div className="card-body">
          <button
            type="button"
            className="btn btn-block btn-primary"
            onClick={() => {
              setIdreceta(idDrink);
              handleOpen();
            }}
          >
            Ver Receta
          </button>
          <Modal
            open={open}
            onClose={() => {
              handleClose();
              setUnareceta({});
            }}
          >
            <div style={modalStyle} className={classes.paper}>
              <h2>{unareceta.strDrink}</h2>
              <h3 className="mt-4">Instrucciones</h3>
              <p>
                {!unareceta.strInstructionsES
                  ? unareceta.strInstructions
                  : unareceta.strInstructionsES}
              </p>
              <img
                className="img-fluid my-4"
                src={unareceta.strDrinkThumb}
                alt={unareceta.strDrink}
              />
              <h3>Ingredientes y cantidades</h3>
              <ul>{mostrarIngredientes(unareceta)}</ul>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default Receta;
